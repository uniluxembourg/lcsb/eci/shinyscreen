FROM registry.gitlab.com/uniluxembourg/lcsb/eci/shinyscreen/dep/ssuser:latest
LABEL maintainer="emma.schymanski@uni.lu"

# Expose port
EXPOSE 3838

# Environment variables
ENV SS_MF_DB="PubChemLite_exposomics.csv" \
    SS_CPU=2

# Copy application code to the container
COPY . shinyscreen/

# Build the R package
RUN R CMD build shinyscreen

# Switch to root user
USER root

# Install the R package
RUN R CMD INSTALL shinyscreen

# Switch to ssuser
USER ssuser

# Prepare the runme script
RUN cp shinyscreen/runme /home/ssuser/runme
RUN chmod u+x /home/ssuser/runme

# Initialize R environment
RUN R -e 'library(shinyscreen);setwd("~");init(top_data_dir="~/top_data_dir",projects="~/projects",users_dir="~/users",metfrag_db_dir=Sys.getenv("SS_MF_DB_DIR"),metfrag_jar="/usr/local/bin/MetFragCommandLine.jar",no_structure_plots=T,save=T,merge=F)'

# Entrypoint and command
ENTRYPOINT ["/home/ssuser/runme"]
CMD ["serve"]

